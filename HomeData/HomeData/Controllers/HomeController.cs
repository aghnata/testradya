﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HomeData.Models;

namespace HomeData.Controllers
{
    public class HomeController : Controller
    {
        private HomeDataDbContext db = new HomeDataDbContext();

        public ActionResult Index()
        {
            var homeData = new ViewModel();
            homeData.Homes = db.Homes.ToList();

            return View(homeData);
        }

        public ActionResult InputViaMap()
        {
            return View();
        }

        //function to add data home
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddData(FormCollection collection)
        {
            string owner = String.Format("{0}", Request.Form["owner"]);
            string type = String.Format("{0}", Request.Form["type"]);
            string lattitude = String.Format("{0}", Request.Form["lattitude"]);
            string longitude = String.Format("{0}", Request.Form["longitude"]);
            string address = String.Format("{0}", Request.Form["address"]);
            int typeInt = int.Parse(type);


            //create baseline
            if (address == "input_via_map")
            {
                float lattitudeFloat = float.Parse(lattitude);
                float longitudeFloat = float.Parse(longitude);
                List<object> lst = new List<object>();
                lst.Add(owner);
                lst.Add(typeInt);
                lst.Add(lattitudeFloat);
                lst.Add(longitudeFloat);
                object[] allitems = lst.ToArray();
                int output = db.Database.ExecuteSqlCommand("insert into Homes(name, type, lattitude, longitude) values(@p0,@p1,@p2,@p3)", allitems);
            } else
            {
                List<object> lst = new List<object>();
                lst.Add(owner);
                lst.Add(typeInt);
                lst.Add(address);
                object[] allitems = lst.ToArray();
                int output = db.Database.ExecuteSqlCommand("insert into Homes(name, type, address) values(@p0,@p1,@p2)", allitems);
            }

            return Redirect("~/Home/Index");
        }

        //function to delete data home
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(FormCollection collection)
        {
            string id = String.Format("{0}", Request.Form["id"]);
            int idInt = int.Parse(id);

            var home = db.Homes.Find(idInt);
            db.Homes.Remove(home);
            db.SaveChanges();

            return Redirect("/");
        }

        //function to edit data
        public ActionResult Edit(int? id)
        {
            Home home = db.Homes.Find(id);

            return View(home);
        }

        //function to update data home
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(FormCollection collection)
        {
            string id = String.Format("{0}", Request.Form["id"]);
            int idInt = int.Parse(id);
            string owner = String.Format("{0}", Request.Form["owner"]);
            string type = String.Format("{0}", Request.Form["type"]);
            string address = String.Format("{0}", Request.Form["address"]);
            int typeInt = int.Parse(type);

            var home = db.Homes.Find(idInt);
            home.name = owner;
            home.type = typeInt;
            home.address = address;
            db.SaveChanges();

            return Redirect("/");
        }

    }
}