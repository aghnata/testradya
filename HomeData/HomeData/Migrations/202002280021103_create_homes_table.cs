﻿namespace HomeData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create_homes_table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Homes",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        type = c.Int(),
                        address = c.String(),
                        longitude = c.Single(),
                        lattitude = c.Single(),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Homes");
        }
    }
}
