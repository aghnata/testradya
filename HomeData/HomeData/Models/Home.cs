﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HomeData.Models
{
    public class Home
    {
        public int? id { get; set; }
        public string name { get; set; }
        public int? type { get; set; }
        public string address { get; set; }
        public float? longitude { get; set; }
        public float? lattitude { get; set; }
    }
}