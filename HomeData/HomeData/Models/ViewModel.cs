﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace HomeData.Models
{
    public class ViewModel
    {
        private HomeDataDbContext db = new HomeDataDbContext();
        public ICollection<Home> Homes { get; set; }
    }
}