﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Entity;


namespace HomeData.Models
{
    public class HomeDataDbContext : DbContext
    {
        public DbSet<Home> Homes { get; set; }
    }
}